import serial
import time
import usbtmc
import datetime
import matplotlib.pyplot as plt
import sys
import setchan

def arduinoCom():
    arduino = serial.Serial('/dev/ttyUSB0', 9600)
    channel = 0
    while (channel < 256):
        arduino.write("+\n")
        channel += 1
        print channel
        time.sleep(0.2)


class Digitizer:
    def __init__(self):
        resource = usbtmc.list_resources()[0]
        self.instrument = usbtmc.Instrument((int)(resource.split("::")[1]), (int)(resource.split("::")[2]))
        print self.instrument.ask("*IDN?")

    def delayedWrite(self, command):
        time.sleep(.01)
        self.instrument.write(command)
        time.sleep(.01)

    def gather(self):
        if (0):
            self.delayedWrite(":SYSTEM:HEADER OFF")  # Controls whether response contains command header
            self.delayedWrite(":WAVEFORM:SOURCE CHANNEL1")  # Output data channel
            self.delayedWrite(":WAVEFORM:FORMAT ASCII")  # Output data Format

            # self.delayedWrite("ACQUIRE:POINTS:AUTO")   #Precision of numbers?
            # self.delayedWrite("ACQuire:AVER OFF")
            self.delayedWrite("ACQUIRE:AVERAGE ON")
            self.delayedWrite("ACQUIRE:AVERAGE:COUNT 16")
            self.delayedWrite("ACQuire:MODE RTIME")
            # self.delayedWrite(":CHANNEL3:INPut DC")
            # self.delayedWrite(":TRIGger:AND:SOURce CHANNEL2")
            # self.delayedWrite("TRIGger:HTHReshold CHANnel2 ")

            self.delayedWrite(":DIGITIZE CHANNEL1")  # Collect data
            self.delayedWrite(":WAVEFORM:DATA?")
            self.delayedWrite(':MEASURE:RESULTS?')

            time.sleep(.1)
            data = self.instrument.read()

            self.delayedWrite(":RUN")
            for i in range(4):
                self.delayedWrite("CHANNEL" + str(i + 1) + ":DISPLAY ON")
                self.delayedWrite(":CHANnel" + str(i + 1) + ":DISPlay:OFFSet 0")


        self.delayedWrite(":SYSTEM:HEADER OFF")
        self.delayedWrite(":ACQuire:MODE RTIME")
        self.delayedWrite(":ACQuire:COMPlete 100")
        self.delayedWrite(":WAVeform:SOURce CHANnel1")
        self.delayedWrite(":WAVEFORM:FORMAT ASCII")
        self.delayedWrite("ACQUIRE:AVERAGE ON")
        self.delayedWrite(":ACQuire:COUNt 8")
        #self.delayedWrite(":ACQuire:POINts 50000")
        self.delayedWrite(":DIGitize CHANnel1")
        self.delayedWrite(":WAVeform:DATA?")

        time.sleep(.1)
        data = self.instrument.read()

        return data


def printData(dirpath, data, channel):
    import os
    import datetime
    folder = 'data'

    # Make General data directory if it does not exist
    if not os.path.exists(folder):
        os.makedirs(folder)

    # Make Directory for this time slot if it does not exist
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)

    output = open(dirpath + "/rawOutput.csv", 'a')  

    data = str(data).strip('[]')
    #data = str.replace(data, ',', '\n')  # switch to columns
    output.write(str(channel)+'\n')
    output.write(data)
    output.write('\n')
    output.close()


def convertstrflt(data):
    data = data.replace(',\n', '').split(',')
    flist = []
    for i in range(len(data)):
        try:
            data[i] = float(data[i])
        except ValueError:
            data.pop(i)

    # list(map(float, data))

    return data


d = Digitizer()
print sys.argv[1], sys.argv[2], sys.argv[3]
dirpath = 'data/' + sys.argv[1]
arduino = serial.Serial('/dev/ttyUSB0', 9600)
time.sleep(2)
channel = int(sys.argv[2])
setchan.setChannel(str(channel), arduino)
if sys.argv[4] == 'S':
    while (channel < int(sys.argv[3])):
        print channel
        data = d.gather()
        data = convertstrflt(data)
        data = data[0:50000]
        printData(dirpath, data, channel)
        plt.close()
        arduino.write("+\n")
        channel += 1
elif sys.argv[4] == 'P':
    while (channel < int(sys.argv[3])):
        print channel
        data = d.gather()
        data = convertstrflt(data)
        data = data[0:50000]
        printData(dirpath, data, channel)
        plt.close()
        arduino.write("+\n")
        channel += 1
    if channel == int(sys.argv[3]):
        channel = int(sys.argv[5])
        setchan.setChannel(str(channel), arduino)
        while channel < int(sys.argv[6]):
            print channel
            data = d.gather()
            data = convertstrflt(data)
            data = data[0:50000]
            printData(dirpath, data, channel)
            plt.close()
            arduino.write("+\n")
            channel += 1
