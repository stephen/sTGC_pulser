import serial
import time

def connect():
    arduino = serial.Serial('/dev/ttyUSB0', 9600)
    time.sleep(1)
    return arduino

def setChannel(channel, arduino):
    arduino.write(channel+"\n")