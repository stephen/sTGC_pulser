import csv
import os
import QL2ABmap
import QS3ABmap

#Information to tell the code which files should be read
directpath = '/media/ian/7EA29A43A29A0037/sTGC_Pulser/data' #Path to (but not into) Folder
dateS1 = 'Aug_28' #Date of test for Strip P1
dateS2 = dateS1 #Date of test for Strip P2
dateP1 = 'Aug_27' #Date of test for Pad
stgc = 'QL2' #sTGC Type
PC = 'PIVOT' #PIVOT or CONFIRM
quad = '04' #Quad Number
layer = '3' #Layer

StripFile = open(stgc+'-'+PC[0]+'-'+quad[1]+'_Layer'+layer+'StripDB.csv', 'w')
SFwriter = csv.writer(StripFile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
SFwriter.writerow(['channel_gfz']+['channel']+['flag_qc']+['amplitude']+['sfr']+['mean']+['variance']+['flag']+['x1']+['x2']+['integral_ratio'])

PadFile = open(stgc+'-'+PC[0]+'-'+quad[1]+'_Layer'+layer+'PadDB.csv', 'w')
PFwriter = csv.writer(PadFile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
PFwriter.writerow(['channel_gfz']+['channel']+['flag_qc']+['amplitude']+['sfr']+['mean']+['variance']+['flag']+['x1']+['x2']+['integral_ratio'])

WireFile = open(stgc+'-'+PC[0]+'-'+quad[1]+'_Layer'+layer+'WireDB.csv', 'w')
WFwriter = csv.writer(WireFile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
WFwriter.writerow(['channel_gfz']+['channel']+['flag_qc']+['amplitude']+['sfr']+['mean']+['variance']+['flag']+['x1']+['x2']+['integral_ratio'])

#Read and generate database file for all of the strips (P1 and P2)
stp1 = dateS1+'_sTGC_'+stgc+'_'+PC+'-00'+quad+'_Layer'+layer+'_S_GFZP1'
for csv_file in os.listdir(os.path.join(directpath, stp1)):
    if csv_file == 'database.csv':
        with open(os.path.join(directpath, stp1, csv_file)) as DBvalues:
            DBreader = csv.reader(DBvalues, delimiter=',', quotechar='|')
            n = 0
            for line in DBreader:
                if n == 0:
                    n+=1
                else:
                    channel = line[0]
                    for m in range(2,6):
                        for n in range(0,6):
                            if line[m][n] == '.':
                                index = n+3
                                line[m] = line[m][:index]
                                break
                    line[9] = line[9][:8]
                    if stgc == 'QS3':
                        elecID = QS3ABmap.QS3electrodeNumber(int(layer), 'S', 'P1', int(channel))
                    if stgc == 'QL2':
                        elecID = QL2ABmap.QL2electrodeNumber(int(layer), 'S', 'P1', int(channel), PC)
                    SFwriter.writerow([channel+'P1']+[elecID]+[line[1]]+[line[2]]+[line[3]]+[line[4]]+[line[5]]+[line[6]]+[line[7]]+[line[8]]+[line[9]])

stp2 = dateS2+'_sTGC_'+stgc+'_'+PC+'-00'+quad+'_Layer'+layer+'_S_GFZP2'
for csv_file in os.listdir(os.path.join(directpath, stp2)):
    if csv_file == 'database.csv':
        with open(os.path.join(directpath, stp2, csv_file)) as DBvalues:
            DBreader = csv.reader(DBvalues, delimiter=',', quotechar='|')
            n = 0
            for line in DBreader:
                if n == 0:
                    n+=1
                else:
                    channel = line[0]
                    for m in range(2,6):
                        for n in range(0,6):
                            if line[m][n] == '.':
                                index = n+3
                                line[m] = line[m][:index]
                                break
                    line[9] = line[9][:8]
                    if stgc == 'QS3':
                        elecID = QS3ABmap.QS3electrodeNumber(int(layer), 'S', 'P2', int(channel))
                    if stgc == 'QL2':
                        elecID = QL2ABmap.QL2electrodeNumber(int(layer), 'S', 'P2', int(channel), PC)
                    SFwriter.writerow([channel+'P2']+[elecID]+[line[1]]+[line[2]]+[line[3]]+[line[4]]+[line[5]]+[line[6]]+[line[7]]+[line[8]]+[line[9]])

#Read and generate database files for Pads and Wires separately
stp3 = dateP1+'_sTGC_'+stgc+'_'+PC+'-00'+quad+'_Layer'+layer+'_P_GFZP1'
for csv_file in os.listdir(os.path.join(directpath, stp3)):
    if csv_file == 'database.csv':
        with open(os.path.join(directpath, stp3, csv_file)) as DBvalues:
            DBreader = csv.reader(DBvalues, delimiter=',', quotechar='|')
            n = 0
            for line in DBreader:
                if n == 0:
                    n+=1
                else:
                    channel = line[0]
                    for m in range(2,6):
                        for n in range(0,6):
                            if line[m][n] == '.':
                                index = n+3
                                line[m] = line[m][:index]
                                break
                    line[9] = line[9][:8]
                    if int(channel) < 128:
                        if stgc == 'QS3':
                            elecID = QS3ABmap.QS3electrodeNumber(int(layer), 'P', 'P1', int(channel))
                        if stgc == 'QL2':
                            elecID = QL2ABmap.QL2electrodeNumber(int(layer), 'P', 'P1', int(channel), PC)
                        PFwriter.writerow([channel+'P1']+[elecID]+[line[1]]+[line[2]]+[line[3]]+[line[4]]+[line[5]]+[line[6]]+[line[7]]+[line[8]]+[line[9]])
                    if int(channel) > 128:
                        if stgc == 'QS3':
                            elecID = QS3ABmap.QS3electrodeNumber(int(layer), 'P', 'P1', int(channel))
                        if stgc == 'QL2':
                            elecID = QL2ABmap.QL2electrodeNumber(int(layer), 'P', 'P1', int(channel), PC)
                        WFwriter.writerow([channel+'P1']+[elecID]+[line[1]]+[line[2]]+[line[3]]+[line[4]]+[line[5]]+[line[6]]+[line[7]]+[line[8]]+[line[9]])