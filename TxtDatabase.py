import csv
import os

directpath = '/home/atlas/Work/sTGC_pulser/data'
dateS1 = 'Apr_14'
dateS2 = dateS1
dateP1 = 'Apr_13'
stgc = 'QL2'
PC = 'PIVOT'
quad = '01'
layer = '1'

df = open(stgc+'_'+PC[0]+'_'+quad+'-L'+layer+'-Database.txt', 'w')

df.write('\nPulser Test Database File')
df.write('\n')
df.write('\nsTGC: '+stgc)
df.write('\nQuad Number: '+quad)
df.write('\nGas Gap: '+layer)
df.write('\nUnique Identifier:')
df.write('\n')
df.write('\nInput Waveform: Square Wave')
df.write('\nInput Amplitude: 20V')
df.write('\nInput Frequency: 500Hz')
df.write('\n')
df.write('\n')
df.write('\nStrip Adapter Board (SAB) - GFZ: P1')
df.write('\nAB Unique ID: ')
df.write('\n')
df.write('\n')

stp1 = dateS1+'_sTGC_'+stgc+'_'+PC+'-00'+quad+'_Layer'+layer+'_S_GFZP1'

for csv_file in os.listdir(os.path.join(directpath, stp1)):
    if csv_file == 'database.csv':
        with open(os.path.join(directpath, stp1, csv_file)) as DBvalues:
            DBreader = csv.reader(DBvalues, delimiter=',', quotechar='|')
            for line in DBreader:
                df.write('\n%-9s%-11s%-22s%-27s%-22s%-24s'%(line[0], line[2], line[4], line[6], line[8], line[10]))
                #df.write('\n'+line[0]+'         '+line[2]+'             '+line[4]+'         '+line[6]+'         '+line[8]+'         '+line[10]+'')

df.write('\n')
df.write('\n')
df.write('\nStrip Adapter Board (SAB) - GFZ: P2')
df.write('\nAB Unique ID: ')
df.write('\n')
df.write('\n')

stp2 = dateS2+'_sTGC_'+stgc+'_'+PC+'-00'+quad+'_Layer'+layer+'_S_GFZP2'

for csv_file in os.listdir(os.path.join(directpath, stp2)):
    if csv_file == 'database.csv':
        with open(os.path.join(directpath, stp2, csv_file)) as DBvalues:
            DBreader = csv.reader(DBvalues, delimiter=',', quotechar='|')
            for line in DBreader:
                df.write('\n%-9s%-11s%-22s%-27s%-22s%-24s'%(line[0], line[2], line[4], line[6], line[8], line[10]))
                #df.write('\n'+line[0]+'         '+line[2]+'             '+line[4]+'         '+line[6]+'         '+line[8]+'         '+line[10]+'')

df.write('\n')
df.write('\n')
df.write('\nPad Adapter Board (PAB) - GFZ: P1')
df.write('\nAB Unique ID: ')
df.write('\nNOTE: Wire Signals begin at Channel 198')
df.write('\n')
df.write('\n')

stp3 = dateP1+'_sTGC_'+stgc+'_'+PC+'-00'+quad+'_Layer'+layer+'_P_GFZP1'

for csv_file in os.listdir(os.path.join(directpath, stp3)):
    if csv_file == 'database.csv':
        with open(os.path.join(directpath, stp3, csv_file)) as DBvalues:
            DBreader = csv.reader(DBvalues, delimiter=',', quotechar='|')
            for line in DBreader:
                df.write('\n%-9s%-11s%-22s%-27s%-22s%-24s'%(line[0], line[2], line[4], line[6], line[8], line[10]))
                #df.write('\n'+line[0]+'         '+line[2]+'             '+line[4]+'         '+line[6]+'         '+line[8]+'         '+line[10]+'')


df.close()
