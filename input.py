#!/usr/bin/env python
from Tkinter import *
import subprocess
import time
import datetime
import QS3ABmap
import QL2ABmap
import tkMessageBox
from PIL import Image
class Window2: #The Code structure of for the Testing Window of the GUI

    def __init__(self, master):
        frame2 = Frame(master)

        #Entry fields for the information required by the pulser test

        stgcframe = Frame(padx=50, pady=10)
        stgcframe.pack()
        stgclabel = Label(stgcframe, text='sTGC Type:', width=25)
        stgclabel.pack(side=LEFT)
        self.stgc = Entry(stgcframe, width=25)
        self.stgc.pack(side=RIGHT)

        uniqueframe = Frame(padx=50, pady=10)
        uniqueframe.pack()
        uniquelabel = Label(uniqueframe, text='Unique Identifier:', width=25)
        uniquelabel.pack(side=LEFT)
        self.unique = Entry(uniqueframe, width=25)
        self.unique.pack(side=RIGHT)

        layerframe = Frame(padx=50, pady=10)
        layerframe.pack()
        layerlabel = Label(layerframe, text='Layer Number:', width=25)
        layerlabel.pack(side=LEFT)
        self.layer = Entry(layerframe, width=25)
        self.layer.pack(side=RIGHT)

        ABframe = Frame(padx=50, pady=10)
        ABframe.pack()
        ABlabel = Label(ABframe, text='Adapter Board:', width=25)
        ABlabel.pack(side=LEFT)
        self.AB = Entry(ABframe, width=25)
        self.AB.pack(side=RIGHT)

        gfzframe = Frame(padx=50, pady=10)
        gfzframe.pack()
        gfzlabel = Label(gfzframe, text='GFZ Number:', width=25)
        gfzlabel.pack(side=LEFT)
        self.gfz = Entry(gfzframe, width=25)
        self.gfz.pack(side=RIGHT)

        PCframe = Frame(padx=50, pady=10)
        PCframe.pack()
        PClabel = Label(PCframe, text='Pivot or Confirm:', width=25)
        PClabel.pack(side=LEFT)
        self.PC = Entry(PCframe, width=25)
        self.PC.pack(side=RIGHT)

        Rangeframe = Frame(padx=50, pady=10)
        Rangeframe.pack()
        Rangelabel = Label(Rangeframe, text='Test Selected Channels:', width=25)
        Rangelabel.pack(side=LEFT)
        self.range = Entry(Rangeframe, width=25)
        self.range.pack(side=RIGHT)

        Pathframe = Frame(padx=50, pady = 10)
        Pathframe.pack()
        Pathlable = Label(Pathframe, text='Provide A folder To Append:', width=25)
        Pathlable.pack(side=LEFT)
        self.path = Entry(Pathframe, width=25)
        self.path.pack(side=RIGHT)

        #Button to run the pulser test

        runframe = Frame(padx=50, pady=10)
        runframe.pack()
        runlabel = Label(runframe, text='Run the sTGC Pulser Test', width=25)
        runlabel.pack(side=LEFT)
        self.run = Button(runframe, text="RUN", command=self.Run)
        self.run.pack(side=RIGHT)

        #Button to sort pulser data

        sortframe = Frame(padx=50, pady=10)
        sortframe.pack()
        sortlabel = Label(sortframe, text='Sort Pulser Data', width=25)
        sortlabel.pack(side=LEFT)
        self.sort = Button(sortframe, text="SORT", command=self.Sort)
        self.sort.pack(side=RIGHT)
        self.sort.config(state=DISABLED)

        #Button to diplay the GFZ mapping with the results of the sorting

        mapframe = Frame(padx=50, pady=10)
        mapframe.pack()
        maplabel = Label(mapframe,text='Display GFZ Mapping', width=25)
        maplabel.pack(side=LEFT)
        self.mapping = Button(mapframe, text='SHOW', command=self.Display)
        self.mapping.pack(side=RIGHT)
        self.mapping.config(state=DISABLED)

        #Button to open the channel setting window

        chanframe = Frame(padx=50, pady=10) 
        chanframe.pack()
        chanlabel = Label(chanframe, text='Set Channel Number', width=25)
        chanlabel.pack(side=LEFT)
        self.arduino = Button(chanframe, text='OPEN', command=self.ArdChan)
        self.arduino.pack(side=RIGHT)
        self.arduino.config(state=DISABLED)

        #Element location Entry field and button

        channelframe = Frame(padx=50, pady=10)
        channelframe.pack()
        channellabel = Label(channelframe, text='Select Channel to Locate:', width=25)
        channellabel.pack(side=LEFT)
        self.channel = Entry(channelframe, width=25)
        self.channel.pack(side=RIGHT)
        self.channel.config(state=DISABLED)

        locateframe = Frame(padx=50, pady=10)
        locateframe.pack()
        locatelabel = Label(locateframe, text='Locate Channel', width=25)
        locatelabel.pack(side=LEFT)
        self.locate = Button(locateframe, text='LOCATE', command=self.Locate)
        self.locate.pack(side=RIGHT)
        self.locate.config(state=DISABLED)

        # plotframe = Frame(padx=50, pady=10)
        # plotframe.pack()
        # plotlabel = Label(plotframe, text='Display Locator Plot', width=25)
        # plotlabel.pack(side=LEFT)
        # self.plot = Button(plotframe, text='SHOW', command=self.PLOT)
        # self.plot.pack(side=RIGHT)
        # self.plot.config(state=DISABLED)

        #Button to Quit

        quitframe = Frame(padx=50, pady=10)
        quitframe.pack(side=BOTTOM)
        back = Button(quitframe, text="Back", command=frame2.quit)
        back.pack(side=LEFT)

        frame2.pack()

    def Run(self):
        instgc = str(self.stgc.get())
        stgcval = instgc.upper()
        inunique = str(self.unique.get())
        uniqueval = inunique.upper()
        inlayer = str(self.layer.get())
        layerval = inlayer.upper()
        inAB = str(self.AB.get())
        ABval = inAB.upper()
        ingfz = str(self.gfz.get())
        gfzval = ingfz.upper()
        inPC = str(self.PC.get())
        PCval = inPC.upper()
        inRange = self.range.get()
        inPath = self.path.get()
        if PCval[0] == 'P':
            positioning = 'PIVOT'
        elif PCval[0] == 'C':
            positioning = 'CONFIRM'
        else:
            positioning = PCval

        #Check if an output directory is provided: if not then create one
        if len(inPath) == 0:
            self.outputDir =  datetime.datetime.now().strftime("%b_%d_sTGC_"+stgcval+"_"+positioning+"-"+uniqueval+"_Layer"+layerval+"_"+ABval+"_GFZ"+gfzval)
        else:
            self.outputDir = str(inPath)
        
        #Check to see if the Range is empty: If so then conduct a full test
        if len(inRange) == 0:            
            self.channelLow = 0
            self.channelHigh = 0
            self.wirerangeLow = 198 
            self.wirerangeHigh = 236
            if stgcval == 'QS3':
                if(ABval == 'P'): #pad
                    if layerval == '1':
                        self.channelLow = 31 
                        self.channelHigh = 55 
                        self.wirerangeHigh = 235
                    elif layerval == '2':
                        self.channelLow = 31
                        self.channelHigh = 55
                    elif layerval == '3':
                        self.channelLow = 23
                        self.channelHigh = 62
                    elif layerval == '4':
                        self.channelLow = 24
                        self.channelHigh = 63
                elif(ABval == 'S'): #strip
                    if(gfzval == 'P1'):
                        self.channelHigh = 256
                    elif(gfzval == 'P2'):
                        self.channelHigh = 51
            elif stgcval == 'QL2':
                if ABval == 'P':
                    if positioning == 'CONFIRM':
                        if layerval == '1':
                            self.channelLow = 33
                            self.channelHigh = 89
                            self.wirerangeHigh = 246
                        elif layerval == '2':
                            self.channelLow = 39
                            self.channelHigh = 95
                            self.wirerangeHigh = 247
                        elif layerval == '3':
                            self.channelLow = 39
                            self.channelHigh = 95
                            self.wirerangeHigh = 247
                        elif layerval == '4':
                            self.channelLow = 33
                            self.channelHigh = 89
                            self.wirerangeHigh = 246
                    elif positioning == 'PIVOT':
                        if layerval == '1':
                            self.channelLow = 34 
                            self.channelHigh = 90
                            self.wirerangeHigh = 246
                        elif layerval == '2':
                            self.channelLow = 38
                            self.channelHigh = 94
                            self.wirerangeHigh = 247
                        elif layerval == '3':
                            self.channelLow = 29
                            self.channelHigh = 104
                            self.wirerangeHigh = 247
                        elif layerval == '4':
                            self.channelLow = 24
                            self.channelHigh = 99
                            self.wirerangeHigh = 246
                elif ABval == 'S':
                    if gfzval == 'P1':
                        self.channelHigh = 256
                    elif gfzval == 'P2':
                        self.channelHigh = 110                    
            n=0
            pbc = ['sudo', 'python', 'pulserBoardControl.py', str(self.outputDir), str(self.channelLow), str(self.channelHigh), str(ABval), str(self.wirerangeLow), str(self.wirerangeHigh), str(stgcval), str(PCval)]
            try:            
                subprocess.check_call(pbc)         
            except:
                n=1
            if n==0:
                tkMessageBox.showinfo('Pulser Test Status', 'Pulser Test Complete') 
                self.sort.config(state=NORMAL)
            elif n==1:
                tkMessageBox.showinfo('Pulser Test Status', 'Failed To Run Pulser Test')
        else:
            rangeval = str(inRange)
            singleList = rangeval.split(',')
            RangeList = {}
            count = 0
            for value in singleList:
                RangeList[count] = value.split('-')
                count+=1
            for key in RangeList:
                if len(RangeList[key])==2:
                    self.channelLow = int(RangeList[key][0])
                    self.channelHigh = int(RangeList[key][1])+1
                    pbc = ['sudo', 'python', 'pulserBoardControl.py', str(self.outputDir), str(self.channelLow), str(self.channelHigh), 'S']
                    try:            
                        subprocess.check_call(pbc)         
                    except:
                        tkMessageBox.showinfo('Pulser Test Status', 'Failed To Run Pulser Test')
                    self.sort.config(state=NORMAL)
                else:
                    self.channelLow = int(RangeList[key][0])
                    self.channelHigh = int(RangeList[key][0])+1
                    pbc = ['sudo', 'python', 'pulserBoardControl.py', str(self.outputDir), str(self.channelLow), str(self.channelHigh), 'S']
                    try:            
                        subprocess.check_call(pbc)         
                    except:
                        tkMessageBox.showinfo('Pulser Test Status', 'Failed To Run Pulser Test')
                    self.sort.config(state=NORMAL)
            tkMessageBox.showinfo('Pulser Test Status', 'Pulser Test Complete') 
        
    def Sort(self):
        instgc = str(self.stgc.get())
        stgcval = instgc.upper()
        inAB = str(self.AB.get())
        ABval = inAB.upper()
        inPC = str(self.PC.get())
        PCval = inPC.upper()
        if PCval[0] == 'P':
            PCval = 'PIVOT'
        elif PCval[0] == 'C':
            PCval = 'CONFIRM'
        m=0
        cs = ['sudo', 'python', 'channelSorting/PSDVCsorting.py', str(self.outputDir), '0', ABval, str(self.channelHigh), str(self.channelLow), str(stgcval), str(PCval)]
        try:
            subprocess.check_call(cs)
        except:
            m=1
        if m ==0:
            tkMessageBox.showinfo('Sorting Status', 'Pulser Data Sorted')
            self.locate.config(state=NORMAL)
            self.mapping.config(state=NORMAL)
            self.channel.config(state=NORMAL)
            self.arduino.config(state=NORMAL)
        elif m == 1:
            tkMessageBox.showinfo('Sorting Status', 'Failed To Sort Pulser Data')
        
    def Display(self):
        subprocess.call(['sudo', 'python', 'channelSorting/sortedchannelplot.py', 'data/'+str(self.outputDir), str(self.channelLow), str(self.channelHigh)])
    
    def ArdChan(self): #Function to open the set channel window
        subprocess.call(['sudo', 'python', 'ArduinoOptions.py'])

    def Locate(self):
        try:
            instgc = str(self.stgc.get())
            stgcval = instgc.upper()
            inlayer = self.layer.get()
            layerval = int(inlayer)
            inAB = str(self.AB.get())
            ABval = inAB.upper()
            ingfz = str(self.gfz.get())
            gfzval = ingfz.upper()
            inPC = str(self.PC.get())
            PCval = inPC.upper()
            if PCval[0] == 'P':
                PCval = 'PIVOT'
            elif PCval[0] == 'C':
                PCval = 'CONFIRM'
            inchannel = str(self.channel.get())
            chanList = inchannel.split(',')
            for channel in chanList:
                channelval = int(channel)   
                if stgcval == 'QS3':         
                    print('You selected channel %s in %s of the %sAB on layer %s of the %s' %(channelval, gfzval, ABval, layerval, stgcval))
                    QS3ABmap.QS3map(layerval,ABval,gfzval,channelval)
                    #self.plot.config(state=NORMAL)
                elif stgcval == 'QL2':
                    print('You selected channel %s in %s of the %sAB on layer %s of the %s-%s' %(channelval, gfzval, ABval, layerval, stgcval, PCval))
                    QL2ABmap.QL2map(layerval,ABval,gfzval,channelval, PCval)
                    #self.plot.config(state=NORMAL)
                else:
                    print("I'm sorry, There is no mapping function for the %s" %(stgcval))
        except:
            tkMessageBox.showinfo('Location Function', 'Please fill in the required information')
       
    # def PLOT(self):
    #     instgc = str(self.stgc.get())
    #     stgcval = instgc.upper()
    #     inlayer = self.layer.get()
    #     layerval = int(inlayer)
    #     inPC = str(self.PC.get())
    #     PCval = inPC.upper()
    #     if stgcval == 'QS3':
    #         locator = 'Plots/layer%slocate.png' %(layerval)
    #         plt = Image.open(locator)
    #         plt.show()
    #     elif stgcval == 'QL2':
    #         print('Locator plot for the %s not finalized' %(stgcval)) 

root = Tk()
window2 = Window2(root)
root.mainloop()

