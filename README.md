***************************************************

INSTRUCTIONS FOR RUNNING AN STGC PULSER SYSTEM TEST 

***************************************************

NOTE: this document assumes that you have made all the necessary physical connections required for the sTGC pulser system test. This includes all connection to the pulser board, the chamber and the control computer. It also assumes that the equipment being used is similar that of the Carleton University setup. Adjustments may have to be made to the code to account for differences in the equipment.

This system was designed to test the QS3 (and eventually the QL2). Suitable mapping functions will be needed to run the test with other sTGC chambers, but the skeleton of the system should function.

***************************
STEP 1: CONFIGURING THE GUI
***************************

The Graphical User Interface (GUI) can be started by running guitest.py as sudo in the terminal.

The first window, labeled  CONFIGURE is designed to ensure a serial connection is established between the computer and both the arduino and the oscilloscope. First, connection must be established with the arduino, this is done by pressing the TEST button. You will notice that the other options are not functional, this will change as connections are established. If the serial connection with the arduino is functional, the channel option and oscilloscope buttons will become functional. You can establish the connection with the scope by pressing its corresponding TEST button. 

If you press the OPEN button, you will open another window that allows the scope to look at a specified channel of the pulser board. This is done by entering the channel number is the specified field and pressing the SET button. When you have selected the desired channel, you can exit this option by pressing the Back button. This process is optional, but is useful for examining the signal before the test is conducted. 

Once the connections have been established for both the Arduino and scope, the NEXT button will become available and the configuration process is complete. 

************************
STEP 2: DATA ACQUISITION
************************ 

Pressing the NEXT button will open the second window. Here you will have to input all of the relevant information about the test you are conducting. Starting with the sTGC type you are testing (QS3 or QL2), next is the ID number of the specific chamber you are testing. These are used mainly for naming the folder that will contain the pulser data (and for selecting the correct sorting/mapping functions)

The next three fields are required by the system to properly conduct the test. The Layer Number takes an integer between 1 and 4 that corresponds to the gap you are connected to. The Adapter Board takes a single letter (S or P) which corresponds to either the strip or pad adapter board. The GFZ Number is specific to the adapter board you are connected to. If you are testing a pad adapter board, then the only option is P1. If you are testing the strip adapter board, then there are two options: P1 and P2. P1 is located closer to the Notch of the strip adapter board and is completely populated. P2 is only partially populated. Both are labeled on the adapter board. Finally, the option of Pivot or Confirm refers to the positioning of the detector. You can input either the letter (P or C) or the word itself.

Once all of the information is provided, you can start the test by pressing the RUN button. If everything is functional, then system will start the data acquisition process, and print the test parameters in the terminal. It will then begin printing the number of the channel it is testing one at a time. This is a good indication that the test is running properly. If the test runs without an issue, then when it finished it will display the message: “Pulser Test Complete”.

If the system encounters an error, then it will display the message: “Failed to Run Pulser Test”. If this occurs, then you can close the window by pressing the BACK button and reestablish a connection with the scope. Check all of the physical connections, then proceed to the second window and reenter the required information. 

The test will take some time, but after the success message is seen the data acquisition process is complete. 

***************************
STEP 3: SORTING AND MAPPING
***************************

At this point, all of the pulser data had been collected and placed in a folder for the specific test. The next step is to sort the data to determine which channels received a signal and which did not. This is done by pressing the SORT button. The sorting is done with a background process, nothing will be immediately printed to the terminal so there will be no initial indication that the process is being conducted except that the SORT button will appear to remain pressed.

If the sorting occurs without error, then the system will display the message: “Pulser Data Sorted”. Similarly, if the system encounters an error, it will display the message: “Failed to Sort Pulser Data”, and a similar troubleshooting process should be conducted.

During the sorting process, the system may print a channel number and a float value. These are the “hot channels”; the value is the variance of the waveform produced by that channel (which is used by the system to determine if the channel received a signal). If the variance is particularly high, the system prints that channel and its value since a high variance corresponds to a larger signal. These hot channels correspond to the readout elements of the chamber that are close to the support structures and therefore have no resistive coating allowing for a larger signal. It is a good idea to note these channels since they should not change for identical tests between chambers (same layer, adapter board and GFZ), so if they are unusual it is an indication that something is wrong and may need more attention. 

The sorting function also saves plots of the waveform for each individual channel in the same folder that the pulser data is saved, these are useful for analysis and troubleshooting later. You can change the code to stop producing the waveforms by opening input.py. Go to the Sort function definition, in the subprocess change the last input variable from a '0' to a '1'.

After sorting, the GFZ mapping will become available by pressing the SHOW button. This will display a visual representation of the physical locations of the channels on the GFZ, coloured according to the sorting results: if a channel is Green, then it received a signal and is connected properly. If a channel is Red, then it did not receive a signal and may be physically disconnected, this will require further attention. If a signal is Grey, then there is no physical connection on that portion of the GFZ, that is, the region of the GFZ is not populated so there is no data for that channel. The White strip in the middle represents the grounding strip of the GFZ connector. This mapping may be saved or discarded according to user preference, and can be regenerated by pressing the SHOW button again, before closing the window.

********************************
STEP 4: READOUT ELEMENT LOCATING
********************************

If all of the channels of the specific GFZ are connected properly then the test is complete, and another GFZ can be tested. However, if some fail the test, then they need to be examined to determine the issue. 

This starts by finding the desired element’s location on both the adapter board and within the chamber. This is done one at a time by entering the channel number into the locating field and pressing the LOCATE button. This will print into the terminal three things: the mapping parameters (the entered information), the element number within the chamber, and the connector number on the adapter board.

The adapter board connectors are numbered according to their own geometry and may not correspond to the same number as the readout element of the chamber. The readout elements are numbered according the the CERN convention.

If some of the information is missing, the system will display a message prompting you to enter the required info. 

After the readout element and connector numbers are printed, a 2D locator plot of the specific chamber can be displayed by pressing the corresponding SHOW button. This figure will indicate the geometry of the chamber, the location of the readout elements within the chamber, and the connector locations on the adapter board. With this the position of the faulty channel can be determined. 

The plot is only specific to the layer, not the individual channels, so its a good idea to locate every red channel using the LOCATE button before opening the plot (the plot can be opened and closed without consequence, but knowing which channels you are looking for will save time).

***********************
STEP 5: TROUBLESHOOTING
***********************

There may be several reasons that a channel failed the pulser system test. 

The first, and most common issue is with the GFZ connection. If channels are failing, it is often to do with the way the GFZ was connected, and not that they are physically disconnected from the chamber or malfunctioning within the chamber. If several channels that are close in proximity (physically on the GFZ, not consecutive numbers) have all failed, or if a large amount of channels have failed (more than 5), then these are good indications that the GFZ connection is the issue.

To solve this, it is a good idea to record the failed channel numbers, and then go back in the GUI and investigate the individual channels. Open the “Set Channel Number” option of the first window and set the scope to view the first channel, if you see a signal trace on the scope then there is another issue, since an improperly connected GFZ will not register a signal. If no trace is seen, you can attempt to apply physical pressure to the region of the GFZ where the channel is, this should ensure an actual connection is established. If a signal appears, then you know that the issue is with the GFZ. 

You can continue to check the other channels this way, but the only way to actually fix the issue is to disconnect the pulser board and then reconnect it, attempting to maintain a better connection then the previous attempt. You will then have run another test (be sure to input all of the same information) to see if the disconnect/reconnect solved the issue. In order to establish a good connection, be careful to properly align the GFZ in the connection region, and to fasten the screws securely. 

If the signal is visible on the scope when you first examine a failed channel, then the issue is likely a software issue, not a hardware one. It means that the signal is present on the channel, but the sorting function is discounting it. If this is the case, then it is a good idea to examine the waveform plots for that test, and compare the waveform of the failed channel to successful ones, in particular the channels in close proximity to the failed channel. You should compare waveform shapes and amplitudes. If the shape is similar, and the scales are close, then it is likely that the variance threshold of the sorting function is too high. You can simply adjust the threshold and resort the data. 

If the waveform is drastically different from the successful channels, or indeed completely flat, it is an indication that the issue is with the physical connection between the adapter board and the chamber. It is very unlikely that the readout element is malfunctioning within the chamber. If the waveform looks very different or it is significantly smaller, but not completely flat, then it is possible that there is a poor or misaligned connection between the chamber and the adapter board. If the channel is completely flat, then it is possible that the connector is broken or simply not connected. In either case, the troubleshooting procedure is the same.

Assuming the issue is not with the GFZ connection, to proceed you should disconnect the pulser board from the chamber, but continue pulsing the gap, you will need to be able to see a signal to determine if the connection is broken. Once the pulser board is disconnected, you should start by visibly inspecting the connection. If it is visibly broken or poorly connected then you have found the issue.

If it appears that it connection is maintained, you can check the connectivity of the connector by attaching the probe to it. If it is connected you will see a signal on the scope, it wont look the same as the signals you were seeing before since it is not going through the same amount of processing electronics. If you see a signal, compare it to a connector you know to be attached. If the signals look different then it could be that the trace is poorly connected. 

If you connect the probe and see no signal then it is possible that the connection is broken but the brake is not visible. The other possibility is that the readout element itself is malfunctioning. If the element in question is a strip, then there is a simple test to determine if it is malfunctioning; the bare copper of the strip is exposed on the other side of the gap (directly opposite to the adapter board). It is possible to connect the probe to these to examine them, if the bare copper registers a signal, then you know that the issue is with the connection to the adapter board. If the strip shows no signal then the issue is internal. It is a good idea to probe a few connected strips by the bare copper to check for any differences in the signals. 

Unfortunately there is no such external path to the pads. If the connection looks fine but the probe does not register a signal, then the connection may have to be redone. If this does not fix the issue then it is possible that the pad is malfunctioning.












