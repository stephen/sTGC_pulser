#!/usr/bin/env python
from Tkinter import *
import setchan
import usbtmc
import tkMessageBox
import subprocess


class Window: #The code structure for the initial GUI window that establishes the Serial connection between both the Arduino and the Oscilloscope and the control computer

    def __init__(self, master):
        frame = Frame(master)

        quitframe = Frame(padx=50, pady=10) #The QUIT Button
        quitframe.pack(side=BOTTOM)
        self.quit = Button(quitframe, text="Quit", fg='red', command=frame.quit)
        self.quit.pack(side=LEFT)
        
        configframe = Frame(padx=50, pady=10)
        configframe.pack()
        configlabel = Label(configframe, text='CONFIGURE', width=25)
        configlabel.pack()

        arduinoframe = Frame(padx=50,pady=10) #The Button to Connect to the Arduino
        arduinoframe.pack()
        ardlabel = Label(arduinoframe, text='Connect To Arduino', width=25)
        ardlabel.pack(side=LEFT)
        ardbutton = Button(arduinoframe, text='Test', command=self.Arduino)
        ardbutton.pack(side=RIGHT)

        chanframe = Frame(padx=50, pady=10) #The Button to open the channel setting window
        chanframe.pack()
        chanlabel = Label(chanframe, text='Set Channel Number', width=25)
        chanlabel.pack(side=LEFT)
        self.chanbutton = Button(chanframe, text='Open', command=self.ArdChan)
        self.chanbutton.pack(side=RIGHT)
        self.chanbutton.config(state=DISABLED)

        scopeframe = Frame(padx=50, pady=10) #The Button to Connect to the Scope
        scopeframe.pack()
        osclabel = Label(scopeframe, text='Connect To Oscilloscope', width=25)
        osclabel.pack(side=LEFT)
        self.oscbutton = Button(scopeframe, text='Test', command=self.Oscilloscope)
        self.oscbutton.pack(side=RIGHT)
        self.oscbutton.config(state=DISABLED)

        self.nextbutton = Button(quitframe, text='Next', command=self.Next) #The Button to open the Testing Window
        self.nextbutton.pack(side=RIGHT)
        self.nextbutton.config(state=DISABLED)

        frame.pack()

    def Arduino(self): #Function that establishes the connection with the Arduino
        n=0
        try:
            self.arduino = setchan.connect()
        except:
            n=1

        if n==0:
            tkMessageBox.showinfo('Arduino Connection', 'Arduino Connected Successfully')
            self.oscbutton.config(state=NORMAL)
            self.chanbutton.config(state=NORMAL)
        else:
            tkMessageBox.showinfo('Arduino Connection', 'Arduino Failed to Connect')

    def ArdChan(self): #Function to open the set channel window
        subprocess.call(['sudo', 'python', 'ArduinoOptions.py'])
        

    def Oscilloscope(self): #Function that establishes the connection with the Scope
        m = 0
        try:
            resource = usbtmc.list_resources()[0]
            self.instrument = usbtmc.Instrument((int)(resource.split("::")[1]),(int)(resource.split("::")[2]))
        except:
            m=1
        if m == 0:
            tkMessageBox.showinfo('Oscilloscope Connection', 'Oscilloscope Connected Successfully')
            self.nextbutton.config(state=NORMAL)
        else:
            tkMessageBox.showinfo('Oscilloscope Connection', 'Oscilloscope Failed to Connect')
            
    def Next(self): #Function to open the testing window
        subprocess.call(['sudo', 'python', 'input.py'])

root = Tk()
window = Window(root)
root.mainloop()

