# Pulse shape determination + Variance comparison
import sys
import csv
import statistics
import numpy as np
import matplotlib.pyplot as plt

outFile = open('data/'+sys.argv[1]+'/sortedpulserdata.csv', 'w')    #Write to CSV for GFZ Mapping
outFileWriter = csv.writer(outFile, delimiter=' ', quoting=csv.QUOTE_MINIMAL)

DatabaseFile = open('data/'+sys.argv[1]+'/database.csv', 'w')   #Write to CSV for database file containing meta-data
DBwriter = csv.writer(DatabaseFile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
DBwriter.writerow(['Channel']+[' ']+['Pass/Fail']+[' ']+['Amplitude']+[' ']+['Sorting Function Response']+[' ']+['Mean']+[' ']+['Variance'])

if sys.argv[6]=='QS3':
    if sys.argv[3] == 'S':
        channelThreshold = 0.01   #SFR threshold used to measure sgnal response
        high = 0.06    #Amplitude value to determine hot strip channels
        low = 0.03
    elif sys.argv[3] == 'P':
        channelThreshold = 0.01   #SFR threshold used to measure sgnal response
        high = 0.08    #SFR value to determine hot pad channels
        low = 0.03
elif sys.argv[6]=='QL2': #THESE VALUES WILL HAVE TO BE CHANGED ONCE THEY HAVE BEEM EXPERIMENTALLY VERIFIED
    channelThreshold = 0.015
    if sys.argv[3] == 'S':
        high = 0.09
        low = 0.04
    elif sys.argv[3] == 'P':
        high = 0.12
        low = 0.03
makePlots = sys.argv[2]

def running_mean(seq, window_size):
    return np.convolve(seq, np.ones((window_size,))/window_size, mode='valid')  #Averaging function to smooth the waveform


with open('data/'+sys.argv[1]+'/rawOutput.csv') as inputRawValues:
    rawReader = csv.reader(inputRawValues, delimiter=',', quotechar='|')    #read raw data from CSV
    channel = 0
    chan = {}
    Vpp = {}
    response = {}
    variances = {}
    averages = {}
    Result = {}
    for line in rawReader:
        if len(line) == 1:  # channel number line, only one element
            channel = int(line[0])  # get channel number
            chan[channel] = []
        else:
            values = [float(i) for i in line]  # convert strings to floats
            chan[channel] = values
        
    for channel in chan:
        if makePlots == '0':
            plt.plot(chan[channel])
            plt.xlabel('Time (arbitrary units)')
            plt.ylabel('Voltage (V)')
            plt.grid(True)
            plt.savefig('data/' + sys.argv[1] + '/channel' + str(channel) + 'Raw.png')
            plt.close()
        waveform = running_mean(chan[channel], 1000)
        Vpp[channel] = max(waveform) - min(waveform) #determine amplitude of the signal
        peaks = 0
        noise = 0
        if makePlots == '0':
            plt.plot(waveform)  #Generate the plot of the smoothed waveform
            plt.xlabel('Time (arbitrary units)')
            plt.ylabel('Voltage (V)')
            plt.grid(True)
            plt.savefig('data/' + sys.argv[1] + '/channel' + str(channel) + 'Smoothed.png')
            plt.close()
        avg = statistics.mean(waveform) #MOVED INITIAL AVERAGE AND VAIRANCE CALCULATION AFTER THE SMOOTHING
        var = np.var(waveform)
        variances[channel] = var
        averages[channel] = avg
        means = []
        for i in range(0, 12):
            total = 0
            for j in range(4000*i, 4000*i+4000):
                total = total+waveform[j]   #Running window for section means
            average = total/4000
            means.append(average)
            ratio = abs(average/avg) #Pulse shape determination 
            if 1.25 > ratio > 0.75:
                noise += 1
            elif ratio > 1.25 or ratio < 0.75:
                peaks += 1
        varmean = np.var(means)
        response[channel] = 5*varmean/Vpp[channel] #SFR or Quality Factor
        if peaks > noise:
            if response[channel] >= channelThreshold:
                if Vpp[channel] > high:
                    outFileWriter.writerow([int(channel)]+[4])
                    Result[channel] = 'PASS'
                elif Vpp[channel] < low:
                    outFileWriter.writerow([int(channel)]+[2])                    
                    Result[channel] = 'PASS'
                elif low <= Vpp[channel] <= high:
                    outFileWriter.writerow([int(channel)]+[3])
                    Result[channel] = 'PASS'
            elif response[channel] < channelThreshold:
                outFileWriter.writerow([int(channel)]+[1])
                Result[channel] = 'FAIL'
            else:
                outFileWriter.writerow([int(channel)]+[1])
                Result[channel] = 'FAIL'
        elif noise > peaks:
            if Vpp[channel] >= high:
                outFileWriter.writerow([int(channel)]+[1])
                Result[channel] = 'FAIL'
            elif response[channel] <= channelThreshold:
                outFileWriter.writerow([int(channel)]+[0])
                print 'channel', channel, 'failed'
                Result[channel] = 'FAIL'
            else:
                outFileWriter.writerow([int(channel)]+[1])
                Result[channel] = 'FAIL'
        else: 
            outFileWriter.writerow([int(channel)]+[1])
            Result[channel] = 'FAIL'
    # #Cut repeadted data to use only latest version
    # if sys.argv[8] == 'QS3':
    #     if sys.argv[3] == 'S':
    #         if sys.argv[4] == '51':
    #             if len(chan) > 51:
    #                 chan = chan[-51:]
    #         elif sys.argv[4] == '256':
    #             if len(chan) > 256:
    #                 chan = chan[-256:]
    #     elif sys.argv[3] == 'P':
    #         numpad = int(sys.argv[4]) - int(sys.argv[5])
    #         numwire = int(sys.argv[7]) - int(sys.argv[6])
    #         pabrange = numpad + numwire
    #         if len(chan) > pabrange:
    #             chan = chan[-pabrange:]
    # elif sys.argv[8] == 'QL2':
    #     if sys.argv[3] == 'S':
    #         if sys.argv[4] == '110':
    #             if len(chan) > 110:
    #                 chan = chan[-110:]
    #         elif sys.argv[4] == '256':
    #             if len(chan) > 256:
    #                 chan = chan[-256:]
    #     elif sys.argv[3] == 'P':
    #         numpad = int(sys.argv[4]) - int(sys.argv[5])
    #         numwire = int(sys.argv[7]) - int(sys.argv[6])
    #         pabrange = numpad + numwire -1
    #         if len(chan) > pabrange:
    #             chan = chan[-pabrange:]        
    xs = []
    Amplitude = []
    Threshold = []
    Variance = []
    for channel in chan:
        DBwriter.writerow([channel]+[' ']+[Result[channel]]+[' ']+[Vpp[channel]]+[' ']+[response[channel]]+[' ']+[averages[channel]]+[' ']+[variances[channel]])
        xs.append(channel)
        Amplitude.append(Vpp[channel])
        Threshold.append(response[channel])
        Variance.append(variances[channel])
    #Create amplitude distribution plot
    plt.plot(xs, Amplitude, label='Signal Amplitude')
    plt.plot(xs, Variance, label='Signal Variance')
    plt.plot(xs, Threshold, label='Sorting Function Response')
    plt.xlabel('Readout Element (GFZ Channel Number)')
    plt.ylabel('Voltage (V)')
    plt.grid(True)
    plt.savefig('data/' + sys.argv[1] + '/AmpDist.png')
    plt.show()
