# Pulse shape determination + Variance comparison
import csv
import statistics
import numpy as np
import matplotlib.pyplot as plt

outFile = open('sortedpulserdata.csv', 'w')
outFileWriter = csv.writer(outFile, delimiter=' ', quoting=csv.QUOTE_MINIMAL)
makePlots = '1'
makeSmoothed = '1'

DatabaseFile = open('database.csv', 'w')
DBwriter = csv.writer(DatabaseFile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
DBwriter.writerow(['Channel']+['Pass/Fail']+['Amplitude']+['Sorting Function Response']+['Mean']+['Variance']+['Weizmann QC Flag']+['X1']+['X2']+['Integral Ratio'])

def running_mean(seq, window_size): #Smoothing Function
    return np.convolve(seq, np.ones((window_size,))/window_size, mode='valid')

def weizmann_test(waveform, channel, averages): #Test to compute Weizmann sorting metrics
    WT = []
    deriv = running_mean(np.diff(waveform), 250)
    x1 = int(np.argmax(deriv[:5001]))
    WT.append(x1)
    x2 = int(np.argmin(deriv[5001:]))+5001
    WT.append(x2)
    integral = [x-averages[channel]for x in waveform[x1:x2]]
    integralVal = np.trapz(integral)
    boxintegral = max(integral)*(x2-x1)
    integral_ratio = integralVal/boxintegral
    WT.append(integral_ratio)
    return WT

def weizmann_sort(WT, channel, Vpp, averages): #Optional test to sort data using Weizmann cuts
    vppLow=10
    vppHigh=80
    offset = averages[channel]
    vpp = Vpp[channel]
    x1 = WT[0]
    x2 = WT[1]
    IR = WT[2]
    if vpp<vppLow:
        amp = 'low'
    elif vpp<vppHigh:
        amp = 'medium'
    else:
        amp = 'high'
    if not 20<offset<40:
        shape = 'bad'
    elif x1 < 2000:
        shape = 'bad'
    elif x2 < 29775:
        shape = 'bad'
    elif IR < 1.0002:
        shape = 'bad'
    else:
        shape = 'good'
    return "%s_shape_%s_amp"%(shape,amp) 

with open('rawOutput.csv') as inputRawValues: #Read source output file from current directory
    rawReader = csv.reader(inputRawValues, delimiter=',', quotechar='|')
    channel = 0
    chan = {}
    Vpp = {}
    response = {}
    variances = {}
    averages = {}
    Result = {}
    Wqcf = {}
    Wx1 = {}
    Wx2 = {}
    WIR = {}
    channelThreshold = 0.01*1000
    high = 0.08*1000
    low = 0.03*1000

    for line in rawReader:
        if len(line) == 1:  # channel number line, only one element
            channel = int(line[0])  # get channel number
            chan[channel] = []
        else:
            values = [float(i) for i in line]  # convert strings to floats
            chan[channel] = values
            
    for channel in chan:
        #Generate Raw Waveform Plots if 0
        if makePlots == '0':
            plt.plot(chan[channel])
            plt.xlabel('Time (arbitrary units)')
            plt.ylabel('Voltage (V)')
            plt.grid(True)
            plt.savefig('channel' + str(channel) + 'Raw.png')
            plt.close()
        #Smooth Signal and collect Vpp, Mean, and Variance
        smoothedsignal = running_mean(chan[channel], 1000)
        Vpp[channel] = (max(smoothedsignal) - min(smoothedsignal))*1000
        avg = statistics.mean(smoothedsignal*1000)
        var = np.var(smoothedsignal*1000)
        variances[channel] = var
        averages[channel] = avg
        peaks = 0
        noise = 0
        #Generate Smoothed Waveform Plots if 0
        if makeSmoothed == '0':
            plt.plot(smoothedsignal*1000)
            plt.xlabel('Time (arbitrary units)')
            plt.ylabel('Voltage (mV)')
            plt.grid(True)
            plt.savefig('channel' + str(channel) + 'Smoothed.png')
            plt.close()
        means = []
        #Compute Carleton Sorting Metrics
        for i in range(0, 12):
            total = 0
            for j in range(4000*i, 4000*i+4000):
                total = total+smoothedsignal[j]
            average = total/4000
            means.append(average*1000)
            ratio = abs(average/avg)
            if 1.25 > ratio > 0.75:
                noise += 1
            elif ratio > 1.25:
                peaks += 1
            elif ratio < 0.75: 
                peaks += 1
        varmean = np.var(means*1000)
        response[channel] = 5*varmean/Vpp[channel]
        #Compute Weizmann Sorting Metrics
        weizmann_list = weizmann_test(smoothedsignal, channel, averages)
        Wx1[channel] = weizmann_list[0]
        Wx2[channel] = weizmann_list[1]
        WIR[channel] = weizmann_list[2]
        #Sort Based on Carleton Metrics and Integral Ratio
        if response[channel] >= channelThreshold:
            if weizmann_list[2] < 1.0002:
                shape = 'bad'
            else:
                shape = 'good'
        else:
            shape = 'bad'
        if Vpp[channel] >= high:
            amp = 'high'
        elif low < Vpp[channel] < high:
            amp = 'medium'
        else: 
            amp = 'low'
        Wqcf[channel] = "%s_shape_%s_amp"%(shape,amp)
        #Sort data for the GFZ Mapping Figure (Same criteria)
        if peaks > noise:
            if response[channel] >= channelThreshold:
                if Vpp[channel] > high:
                    outFileWriter.writerow([int(channel)]+[4])
                    Result[channel] = 'PASS'
                elif Vpp[channel] < low:
                    outFileWriter.writerow([int(channel)]+[2])                    
                    Result[channel] = 'PASS'
                elif low <= Vpp[channel] <= high:
                    outFileWriter.writerow([int(channel)]+[3])
                    Result[channel] = 'PASS'
            elif response[channel] < channelThreshold:
                outFileWriter.writerow([int(channel)]+[1])
                Result[channel] = 'FAIL'
            else:
                outFileWriter.writerow([int(channel)]+[1])
                Result[channel] = 'FAIL'
        elif noise > peaks:
            if Vpp[channel] > high: 
                outFileWriter.writerow([int(channel)]+[1])
                Result[channel] = 'FAIL'
            elif response[channel] <= channelThreshold:
                outFileWriter.writerow([int(channel)]+[0])
                Result[channel] = 'FAIL'
            else:
                outFileWriter.writerow([int(channel)]+[1])
                Result[channel] = 'FAIL'
        else: 
            outFileWriter.writerow([int(channel)]+[1])
            Result[channel] = 'FAIL'   

    #Fill database.csv with sorted values
    xs = []
    Amplitude = []
    Threshold = []
    Variance = []
    wx1 = []
    wx2 = []
    wir = []
    offset = []
    for channel in chan:
        DBwriter.writerow([channel]+[Result[channel]]+[Vpp[channel]]+[response[channel]]+[averages[channel]]+[variances[channel]]+[Wqcf[channel]]+[Wx1[channel]]+[Wx2[channel]]+[WIR[channel]])
        xs.append(channel)
        Amplitude.append(Vpp[channel])
        Threshold.append(response[channel])
        Variance.append(variances[channel])
        wx1.append(Wx1[channel])
        wx2.append(Wx2[channel])
        wir.append(WIR[channel]) 
        offset.append(averages[channel])

    #Generate Plots 
    plt.figure()
    plt.subplot(311)
    plt.plot(xs, Amplitude, 'r-')
    plt.xlabel('Readout Element (GFZ Channel Number)')
    plt.ylabel('Amplitude (mV)')
    plt.grid(True)

    plt.subplot(312)
    plt.plot(xs, wir, 'c-')
    plt.xlabel('Readout Element (GFZ Channel Number)')
    plt.ylabel('Integral Ratio')
    plt.grid(True)

    plt.subplot(313)
    plt.plot(xs, Threshold, 'g-')
    plt.xlabel('Readout Element (GFZ Channel Number)')
    plt.ylabel('S.F.R. (mV)')
    plt.grid(True)

    plt.savefig('AmpDist.png')
    plt.show()

    plt.figure()
    plt.subplot(311)
    plt.plot(xs, wx1, 'b-')
    plt.xlabel('Readout Element (GFZ Channel Number)')
    plt.ylabel('X1')
    plt.grid(True)

    plt.subplot(312)
    plt.plot(xs, wx2, 'k-')
    plt.xlabel('Readout Element (GFZ Channel Number)')
    plt.ylabel('X2')
    plt.grid(True)

    plt.subplot(313)
    plt.plot(xs, offset, 'm-')
    plt.xlabel('Readout Element (GFZ Channel Number)')
    plt.ylabel('Mean (mV)')
    plt.grid(True)
    
    plt.savefig('WeizDist.png')
    plt.show()