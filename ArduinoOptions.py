#!/usr/bin/env python
from Tkinter import *
import setchan
import tkMessageBox
import subprocess

class Ardwin: #The code structure for the GUI window that allows you to select a channel on the GFZ using the Arduino

    def __init__(self, master):
        frame3 = Frame(master)

        optionframe = Frame(padx=50, pady=10) #The Channel Entry Field and SET button
        optionframe.pack()
        setlabel = Label(optionframe, text='Enter Channel Number:', width=25)
        setlabel.pack(side=LEFT)
        setbutton = Button(optionframe, text='Set', command=self.Set)
        setbutton.pack(side=RIGHT, padx=10)
        self.set = Entry(optionframe, width=15)
        self.set.pack(side=RIGHT)

        quitframe = Frame(padx=50, pady=10) #The DONE button
        quitframe.pack(side=BOTTOM)
        back = Button(quitframe, text="Done", command=frame3.quit)
        back.pack(side=LEFT)

        frame3.pack()

    def Set(self): #the function that calls setchan.py in order to select the channel
        self.arduino = setchan.connect()
        inset = str(self.set.get())
        if int(inset) in range(0,256): 
            setchan.setChannel(inset, self.arduino)
            print('Channel Set to %s' %(inset))
        
        
root = Tk()
arduinowindow = Ardwin(root)
root.mainloop()
